function eventChange1() {
    var value = document.getElementById("inputGroupSelect01").value;
    var rate = $('#inputGroupSelect01').find(':selected').data('rate');

    document.getElementById("currency1Name").value = value;
    document.getElementById("currency1Rate").value = rate;
}

function eventChange2() {
    var value = document.getElementById("inputGroupSelect02").value;
    var rate = $('#inputGroupSelect02').find(':selected').data('rate');


    document.getElementById("currency2Name").value = value;
    document.getElementById("currency2Rate").value = rate;
}

function revers() {
    var one = document.getElementById("inputGroupSelect01").value;
    var two = document.getElementById("inputGroupSelect02").value;
    $('#inputGroupSelect01').val(two).change();
    $('#inputGroupSelect02').val(one).change();
}

function getNowDate() {
    var today = new Date();
    var dd = String(today.getDate()).padStart(2, '0');
    var mm = String(today.getMonth() + 1).padStart(2, '0');
    var yyyy = today.getFullYear();

    today = dd + '/' + mm + '/' + yyyy;
    return today;
}

function sandData() {
    var obj = {};
    var objCurr1 = {};
    var objCurr2 = {};

    var form = document.getElementById("formCurrency1");
    var formData = new FormData(form);

    formData.forEach(function (value, key) {
        objCurr1[key] = value;
    });

    form = document.getElementById("formCurrency2");
    formData = new FormData(form);

    formData.forEach(function (value, key) {
        objCurr2[key] = value;
    });
    obj.currencyOne = objCurr1;
    obj.currencyTwo = objCurr2;
    obj.sumOne = document.getElementById("sumOne").value;
    obj.sumTwo = document.getElementById("sumTwo").value;
    obj.date = getNowDate();
    obj.elem = event.target.id.toString();

    var stringJson = JSON.stringify(obj);
    var xhr = new XMLHttpRequest();

    var url = "/convert/";
    xhr.open("POST", url, false);
    xhr.setRequestHeader("Content-type", "application/json");
    xhr.send(stringJson);
    if (xhr.status !== 200) {
        alert(xhr.status + ': ' + xhr.statusText);
    } else {
        var response = JSON.parse(xhr.responseText);
        if (event.target.id.toString() == "sumOne") {
            document.getElementById("sumTwo").value = response;
        } else if (event.target.id.toString() == "sumTwo") {
            document.getElementById("sumOne").value = response;
        }
    }
}

function isEmpty(obj) {
    for (var prop in obj) {
        if (obj.hasOwnProperty(prop))
            return false;
    }

    return true;
}