package ru.moneyconverter;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MoneyConverter {
    public static void main(String[] args) {
        SpringApplication.run(MoneyConverter.class,args);
        System.out.println("###################!START APP!####################");
    }
}
