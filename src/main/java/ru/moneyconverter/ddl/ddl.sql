create table users_tbl
(
    id_user  serial       not null
        constraint users_tabl_pk
            primary key,
    name     varchar(40)  not null,
    password varchar(200) not null
);

alter table users_tbl
    owner to aleksandr;

create unique index users_tabl_name_uindex
    on users_tbl (name);

create table history_tbl
(
    id_history      serial  not null
        constraint history_tbl_pk
            primary key,
    currency_one_id integer not null,
    currency_two_id integer not null,
    date            date    not null
);

alter table history_tbl
    owner to aleksandr;

create table currencies_tbl
(
    id_currency serial     not null
        constraint currencies_tbl_pk
            primary key,
    charcode    varchar(5) not null,
    nominal     integer    not null,
    name        varchar(100),
    rate        money      not null,
    date        date
);

alter table currencies_tbl
    owner to aleksandr;

