package ru.moneyconverter.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.moneyconverter.models.CurrencyModel;

import java.util.Collection;
import java.util.Date;
import java.util.Optional;

@Repository
public interface CurrencyRepository extends JpaRepository<CurrencyModel, Long> {
    Collection<CurrencyModel> findAllByDate(Date date);

    Optional<CurrencyModel> findFirstByDate(Date date);

    Optional<CurrencyModel> findByCharCodeAndDate(String charCode, Date date);
}
