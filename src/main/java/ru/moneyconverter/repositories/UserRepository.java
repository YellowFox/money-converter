package ru.moneyconverter.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.moneyconverter.models.UserModel;

public interface UserRepository extends JpaRepository<UserModel, Long> {
}
