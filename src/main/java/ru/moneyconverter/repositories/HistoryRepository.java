package ru.moneyconverter.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.moneyconverter.models.HistoryModel;

public interface HistoryRepository extends JpaRepository<HistoryModel, Long> {
}
