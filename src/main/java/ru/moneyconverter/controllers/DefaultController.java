package ru.moneyconverter.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;
import ru.moneyconverter.service.CurrencyService;
import ru.moneyconverter.service.XmlParseService;

@RestController
public class DefaultController {

    private final CurrencyService currencyService;
    private final XmlParseService xmlParseService;

    @Autowired
    public DefaultController(CurrencyService currencyService, XmlParseService xmlParseService) {
        this.currencyService = currencyService;
        this.xmlParseService = xmlParseService;
    }

    @RequestMapping(value = {"/", "/index"})
    public ModelAndView index() {
        ModelAndView index = xmlParseService.processingCurrenciesBeforeView();

        return index;

    }
}
