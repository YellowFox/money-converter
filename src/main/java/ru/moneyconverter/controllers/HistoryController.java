package ru.moneyconverter.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;
import ru.moneyconverter.service.HistoryService;
import ru.moneyconverter.service.dto.HistoryServiceDto;

import java.util.Collection;

@RestController
public class HistoryController {

    @Autowired
    private final HistoryService historyService;

    public HistoryController(HistoryService historyService) {
        this.historyService = historyService;
    }

    @RequestMapping(value = "/history")
    public ModelAndView history(){
        ModelAndView history = new ModelAndView();
        Collection<HistoryServiceDto> historyServiceDtos = historyService.findAll();
        history.addObject("history", historyServiceDtos);
        return history;
    }
}
