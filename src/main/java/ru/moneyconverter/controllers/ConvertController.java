package ru.moneyconverter.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import ru.moneyconverter.controllers.Dto.ConvertDtoWeb;
import ru.moneyconverter.service.ConvertService;
import ru.moneyconverter.service.CurrencyService;
import ru.moneyconverter.service.HistoryService;

@RestController
public class ConvertController {
    private final ConvertService convertService;

    @Autowired
    public ConvertController(ConvertService convertService) {
        this.convertService = convertService;
    }

    @RequestMapping(value = "/convert", produces = "application/json")
    @ResponseBody
    public String convert(@RequestBody ConvertDtoWeb convertDtoWeb){
        return convertService.convert(convertDtoWeb);
    }

}
