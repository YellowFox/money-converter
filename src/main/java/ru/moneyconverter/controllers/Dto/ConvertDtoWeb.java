package ru.moneyconverter.controllers.Dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import ru.moneyconverter.service.dto.CurrencyServiceDto;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Locale;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class ConvertDtoWeb {

    private CurrencyServiceDto currencyOne;
    private CurrencyServiceDto currencyTwo;
    private BigDecimal sumOne;
    private BigDecimal sumTwo;
    private String date;
    private String elem;

}
