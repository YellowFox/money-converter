package ru.moneyconverter.service.convertors;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.stereotype.Component;
import ru.moneyconverter.models.CurrencyModel;
import ru.moneyconverter.service.dto.CurrencyServiceDto;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Optional;

@Data
@NoArgsConstructor
@Component
public class CurrencyConverter {
    public Optional<CurrencyServiceDto> currencyRepositoryToService(Optional<CurrencyModel> currencyModel) {
        CurrencyServiceDto currencyServiceDto = new CurrencyServiceDto();
        if (currencyModel.isPresent()) {
            currencyServiceDto.setId(currencyModel.get().getId());
            currencyServiceDto.setCharCode(currencyModel.get().getCharCode());
            currencyServiceDto.setNominal(currencyModel.get().getNominal());
            currencyServiceDto.setName(currencyModel.get().getName());
            currencyServiceDto.setRate(currencyModel.get().getRate());
            currencyServiceDto.setDate(currencyModel.get().getDate());
        } else {
            return Optional.empty();
        }
        return Optional.of(currencyServiceDto);
    }

    public CurrencyServiceDto currencyRepositoryToService(CurrencyModel currencyModel) {
        CurrencyServiceDto currencyServiceDto = new CurrencyServiceDto();
        if (currencyModel != null) {
            currencyServiceDto.setId(currencyModel.getId());
            currencyServiceDto.setCharCode(currencyModel.getCharCode());
            currencyServiceDto.setNominal(currencyModel.getNominal());
            currencyServiceDto.setName(currencyModel.getName());
            currencyServiceDto.setRate(currencyModel.getRate());
            currencyServiceDto.setDate(currencyModel.getDate());
        } else {
            return null;
        }
        return currencyServiceDto;
    }

    public CurrencyModel currencyServiceToRepository(CurrencyServiceDto currencyServiceDto) {
        CurrencyModel currencyModel = new CurrencyModel();
        if (currencyServiceDto != null) {
            currencyModel.setId(currencyServiceDto.getId());
            currencyModel.setCharCode(currencyServiceDto.getCharCode());
            currencyModel.setNominal(currencyServiceDto.getNominal());
            currencyModel.setName(currencyServiceDto.getName());
            currencyModel.setRate(currencyServiceDto.getRate());
            currencyModel.setDate(currencyServiceDto.getDate());
        } else {
            return null;
        }
        return currencyModel;
    }

    public Collection<CurrencyServiceDto> currencyRepositoryCollectionToService(Collection<CurrencyModel> currencyModelCollection) {
        Collection<CurrencyServiceDto> currencyServiceDtos = new ArrayList<>();
        if (currencyModelCollection.isEmpty()) {
            return currencyServiceDtos;
        }
        for (CurrencyModel curr : currencyModelCollection) {
            currencyServiceDtos.add(new CurrencyServiceDto(curr.getId(), curr.getCharCode(), curr.getNominal(),
                    curr.getName(), curr.getRate(), curr.getDate()));
        }
        return currencyServiceDtos;
    }

    public Collection<CurrencyModel> currencyServiceCollectionToRepository(Collection<CurrencyServiceDto> currencyModelCollection) {
        Collection<CurrencyModel> currencyServiceDtos = new ArrayList<>();
        if (currencyModelCollection.isEmpty()) {
            return currencyServiceDtos;
        }
        for (CurrencyServiceDto curr : currencyModelCollection) {
            currencyServiceDtos.add(new CurrencyModel(curr.getId(), curr.getCharCode(), curr.getNominal(),
                    curr.getName(), curr.getRate(), curr.getDate()));
        }
        return currencyServiceDtos;
    }
}
