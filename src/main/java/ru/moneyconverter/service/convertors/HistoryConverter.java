package ru.moneyconverter.service.convertors;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.moneyconverter.models.HistoryModel;
import ru.moneyconverter.service.dto.HistoryServiceDto;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Optional;

@Data
@Component
public class HistoryConverter {

    private final CurrencyConverter currencyConverter;

    public HistoryConverter(CurrencyConverter currencyConverter) {
        this.currencyConverter = currencyConverter;
    }

    public Optional<HistoryServiceDto> historyRepositoryToHistoryService(Optional<HistoryModel> historyModel) {
        HistoryServiceDto historyServiceDto = new HistoryServiceDto();
        if (historyModel.isPresent()) {
            historyServiceDto.setId(historyModel.get().getId());
            historyServiceDto.setCurrencyOne(currencyConverter.currencyRepositoryToService(historyModel.get().getCurrencyOne()));
            historyServiceDto.setCurrencyTwo(currencyConverter.currencyRepositoryToService(historyModel.get().getCurrencyTwo()));
            historyServiceDto.setDate(historyModel.get().getDate());
        } else {
            return Optional.empty();
        }
        return Optional.of(historyServiceDto);
    }

    public HistoryModel historyServiceToHisRepository(HistoryServiceDto historyServiceDto) {
        HistoryModel historyModel = new HistoryModel();
        if (historyServiceDto.getId() != null) {
            historyModel.setId(historyServiceDto.getId());
            historyModel.setDate(historyServiceDto.getDate());
            historyModel.setCurrencyOne((currencyConverter.currencyServiceToRepository(historyServiceDto.getCurrencyOne())));
            historyModel.setCurrencyTwo((currencyConverter.currencyServiceToRepository(historyServiceDto.getCurrencyTwo())));
            return historyModel;
        } else {
            historyModel.setId(null);
            historyModel.setDate(historyServiceDto.getDate());
            historyModel.setCurrencyOne((currencyConverter.currencyServiceToRepository(historyServiceDto.getCurrencyOne())));
            historyModel.setCurrencyTwo((currencyConverter.currencyServiceToRepository(historyServiceDto.getCurrencyTwo())));
            return historyModel;
        }
    }

    public Collection<HistoryServiceDto> historyRepCollToHisServiceColl
            (Collection<HistoryModel> historyModelCollection) {
        Collection<HistoryServiceDto> historyServiceDtos = new ArrayList<>();
        if (historyModelCollection.isEmpty()) {
            return historyServiceDtos;
        }
        for (HistoryModel his : historyModelCollection) {
            historyServiceDtos.add(new HistoryServiceDto(his.getId(),
                    currencyConverter.currencyRepositoryToService(his.getCurrencyOne()),
                    currencyConverter.currencyRepositoryToService(his.getCurrencyTwo()),
                    his.getDate()));
        }
        return historyServiceDtos;
    }
}
