package ru.moneyconverter.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.moneyconverter.models.CurrencyModel;
import ru.moneyconverter.repositories.CurrencyRepository;
import ru.moneyconverter.service.convertors.CurrencyConverter;
import ru.moneyconverter.service.dto.CurrencyServiceDto;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Collection;
import java.util.Date;
import java.util.Optional;

@Service
public class CurrencyService {

    private final CurrencyRepository currencyRepository;
    private final CurrencyConverter currencyConverter;

    @Autowired
    public CurrencyService(CurrencyRepository currencyRepository, CurrencyConverter currencyConverter) {
        this.currencyRepository = currencyRepository;
        this.currencyConverter = currencyConverter;
    }

    public Optional<CurrencyServiceDto> findById(Long id) {
        Optional<CurrencyModel> currencyModel = currencyRepository.findById(id);
        return currencyConverter.currencyRepositoryToService(currencyModel);
    }

    public Collection<CurrencyServiceDto> findAll() {
        Collection<CurrencyModel> currencyModels = currencyRepository.findAll();
        return currencyConverter.currencyRepositoryCollectionToService(currencyModels);
    }

    public void saveAll(Collection<CurrencyServiceDto> currencyServiceDtos) {
        currencyRepository.saveAll(currencyConverter.currencyServiceCollectionToRepository(currencyServiceDtos));
    }

    public Collection<CurrencyServiceDto> findAllByDate(LocalDateTime localDateTime) {
        return currencyConverter.currencyRepositoryCollectionToService
                (currencyRepository.findAllByDate(Date.from(localDateTime.atZone(ZoneId.systemDefault()).toInstant())));
    }

    public Optional<CurrencyServiceDto> findByCharCodeAndDate(String charCode, LocalDateTime localDateTime) {
        return currencyConverter.currencyRepositoryToService
                (currencyRepository.findByCharCodeAndDate(charCode, Date.from(localDateTime.atZone(ZoneId.systemDefault()).toInstant())));
    }

    public Optional<CurrencyServiceDto> findByCharCodeAndDate(String charCode, Date date) {
        return currencyConverter.currencyRepositoryToService
                (currencyRepository.findByCharCodeAndDate(charCode, date));
    }

    public Optional<CurrencyServiceDto> findFirstByDate(LocalDateTime localDateTime) {
        return currencyConverter.currencyRepositoryToService(
                currencyRepository.findFirstByDate(Date.from(localDateTime.atZone(ZoneId.systemDefault()).toInstant())));
    }

    public static Date convertStringToDate(String dateStr) {
        Date date;
        try {
            date = new SimpleDateFormat("dd/MM/yyyy").parse(dateStr);
            return date;
        } catch (ParseException e) {
            e.printStackTrace();
            System.out.println("Что то не так с датой полученной с сайта");
            date = null;
            return date;
        }
    }

    public static LocalDate convertStringToLocalDate(String dateStr){
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
        return LocalDate.parse(dateStr, formatter);
    }

}
