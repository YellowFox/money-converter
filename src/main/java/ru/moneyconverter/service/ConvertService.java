package ru.moneyconverter.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.moneyconverter.controllers.Dto.ConvertDtoWeb;

import java.math.BigDecimal;
import java.math.RoundingMode;

@Service
public class ConvertService {

    private final HistoryService historyService;

    @Autowired
    public ConvertService(HistoryService historyService) {
        this.historyService = historyService;
    }

    public String convert(ConvertDtoWeb convertDtoWeb) {
        BigDecimal rateOne = convertDtoWeb.getCurrencyOne().getRate();
        BigDecimal rateTwo = convertDtoWeb.getCurrencyTwo().getRate();
        BigDecimal amountOne = convertDtoWeb.getSumOne();
        BigDecimal amountTwo = convertDtoWeb.getSumTwo();
        if (amountOne == null) {
            amountOne = BigDecimal.ZERO;
        }
        if (amountTwo == null) {
            amountTwo = BigDecimal.ZERO;
        }
        BigDecimal curr = new BigDecimal(0);
        if (amountOne.compareTo(BigDecimal.ZERO) != 0 & convertDtoWeb.getElem().equals("sumOne")) {
            BigDecimal rubAmount = rateOne.multiply(amountOne);
            curr = rubAmount.divide(rateTwo, RoundingMode.CEILING);
            historyService.historyPrepareSave(convertDtoWeb);
            return curr.toString();
        } else if (amountTwo.compareTo(BigDecimal.ZERO) != 0 & convertDtoWeb.getElem().equals("sumTwo")) {
            BigDecimal rubAmount = rateTwo.multiply(amountTwo);
            curr = rubAmount.divide(rateOne, RoundingMode.CEILING);
            historyService.historyPrepareSave(convertDtoWeb);
            return curr.toString();
        } else {
            return curr.toString();
        }
    }
}
