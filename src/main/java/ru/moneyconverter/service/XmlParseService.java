package ru.moneyconverter.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.servlet.ModelAndView;
import ru.moneyconverter.service.dto.CurrencyServiceDto;

import javax.xml.namespace.QName;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

@Service
public class XmlParseService {

    private final CurrencyService currencyService;

    @Autowired
    public XmlParseService(CurrencyService currencyService) {
        this.currencyService = currencyService;
    }

    public ModelAndView processingCurrenciesBeforeView() {
        ModelAndView index = new ModelAndView("index");
        LocalDateTime date = LocalDateTime.now();
        Collection<CurrencyServiceDto> currencyServiceDtos;
        if (date.getHour() < 13) {
            currencyServiceDtos = currencyService.findAllByDate(date);
            if (!currencyServiceDtos.isEmpty()) {
                index.addObject("currencies", currencyServiceDtos);
                index.addObject("rub", currencyService.findByCharCodeAndDate("RUB", date));
                return index;
            } else {
                currencyServiceDtos = getAllCurrencyFromCbr();
                currencyService.saveAll(currencyServiceDtos);
                index.addObject("currencies", currencyServiceDtos);
                index.addObject("rub", currencyService.findByCharCodeAndDate("RUB", date));
                return index;
            }
        } else {
            date = date.plusDays(1L);
            currencyServiceDtos = currencyService.findAllByDate(date);
            if (currencyServiceDtos.isEmpty()) {
                currencyServiceDtos = getAllCurrencyFromCbr();
                currencyService.saveAll(currencyServiceDtos);
                currencyServiceDtos = currencyService.findAllByDate(date);
                index.addObject("currencies", currencyServiceDtos);
                index.addObject("rub", currencyService.findByCharCodeAndDate("RUB", date));
                return index;
            } else {
                currencyServiceDtos = currencyService.findAllByDate(date);
                index.addObject("currencies", currencyServiceDtos);
                index.addObject("rub", currencyService.findByCharCodeAndDate("RUB", date));
                return index;
            }
        }
    }

    private Collection<CurrencyServiceDto> getAllCurrencyFromCbr() {
        Collection<CurrencyServiceDto> currencyServiceDtos = new ArrayList<>();
        XMLInputFactory factory = XMLInputFactory.newInstance();
        Date date = null;
        try {
            URL url = new URL("http://www.cbr.ru/scripts/XML_daily.asp");
            InputStream stream = url.openStream();
            XMLEventReader eventReader =
                    factory.createXMLEventReader(stream);
            CurrencyServiceDto currencyServiceDto = new CurrencyServiceDto();
            while (eventReader.hasNext()) {
                XMLEvent nextEvent = eventReader.nextEvent();
                if (nextEvent.isStartElement()) {
                    StartElement startElement = nextEvent.asStartElement();
                    switch (startElement.getName().getLocalPart()) {
                        case "ValCurs": {
                            Attribute dateXml = startElement.getAttributeByName(new QName("Date"));
                            if (dateXml != null) {
                                SimpleDateFormat formatter = new SimpleDateFormat("dd.MM.yyyy");
                                try {
                                    date = formatter.parse(dateXml.getValue());
                                } catch (ParseException e) {
                                    System.out.println("Что то не так с датами в xml!");
                                    e.printStackTrace();
                                }
                            }
                            break;
                        }
                        case "CharCode": {
                            nextEvent = eventReader.nextEvent();
                            currencyServiceDto.setDate(date);
                            currencyServiceDto.setCharCode(nextEvent.asCharacters().getData());
                            break;
                        }
                        case "Nominal": {
                            nextEvent = eventReader.nextEvent();
                            currencyServiceDto.setNominal(Integer.valueOf(nextEvent.asCharacters().getData()));
                            break;
                        }
                        case "Name": {
                            nextEvent = eventReader.nextEvent();
                            currencyServiceDto.setName(nextEvent.asCharacters().getData());
                            break;
                        }
                        case "Value": {
                            nextEvent = eventReader.nextEvent();
                            currencyServiceDto.setRate(new BigDecimal(nextEvent.asCharacters().getData().
                                    replace(",", ".")));
                            break;
                        }
                    }
                }
                if (nextEvent.isEndElement()) {
                    EndElement endElement = nextEvent.asEndElement();
                    if (endElement.getName().getLocalPart().equals("Valute")) {
                        currencyServiceDtos.add(currencyServiceDto);
                        currencyServiceDto = new CurrencyServiceDto();
                    }
                }
            }
        } catch (IOException | XMLStreamException e) {
            System.out.println("Что то не так с url цб или xml изменилась!");
            e.printStackTrace();
        }
        currencyServiceDtos.add(new CurrencyServiceDto(null,"RUB",1,
                "Росссийский рубль",new BigDecimal(1),date));
        return currencyServiceDtos;
    }

}

