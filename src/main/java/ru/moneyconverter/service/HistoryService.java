package ru.moneyconverter.service;

import org.springframework.stereotype.Service;
import ru.moneyconverter.controllers.Dto.ConvertDtoWeb;
import ru.moneyconverter.repositories.HistoryRepository;
import ru.moneyconverter.service.convertors.HistoryConverter;
import ru.moneyconverter.service.dto.CurrencyServiceDto;
import ru.moneyconverter.service.dto.HistoryServiceDto;

import java.util.Collection;
import java.util.Optional;

@Service
public class HistoryService {
    private final HistoryRepository historyRepository;
    private final HistoryConverter historyConverter;
    private final CurrencyService currencyService;

    public HistoryService(HistoryRepository historyRepository, HistoryConverter historyConverter, CurrencyService currencyService) {
        this.historyRepository = historyRepository;
        this.historyConverter = historyConverter;
        this.currencyService = currencyService;
    }

    public Optional<HistoryServiceDto> findById(Long id) {
        return historyConverter.historyRepositoryToHistoryService(historyRepository.findById(id));
    }

    public Collection<HistoryServiceDto> findAll() {
        return historyConverter.historyRepCollToHisServiceColl(historyRepository.findAll());
    }

    public void save(HistoryServiceDto historyServiceDto) {
        historyRepository.save(historyConverter.historyServiceToHisRepository(historyServiceDto));
    }

    public void historyPrepareSave(ConvertDtoWeb convertDtoWeb) {
        HistoryServiceDto historyServiceDto = new HistoryServiceDto();
        Optional<CurrencyServiceDto> currencyServiceDtoOptionalOne =
                currencyService.findByCharCodeAndDate(convertDtoWeb.getCurrencyOne().getCharCode(),
                        CurrencyService.convertStringToDate(convertDtoWeb.getDate()));
        Optional<CurrencyServiceDto> currencyServiceDtoOptionalTwo =
                currencyService.findByCharCodeAndDate(convertDtoWeb.getCurrencyTwo().getCharCode(),
                        CurrencyService.convertStringToDate(convertDtoWeb.getDate()));
        if (currencyServiceDtoOptionalOne.isPresent() &&
                currencyServiceDtoOptionalTwo.isPresent()) {
            historyServiceDto.setCurrencyOne(currencyServiceDtoOptionalOne.get());
            historyServiceDto.setCurrencyTwo(currencyServiceDtoOptionalTwo.get());
        }
        historyServiceDto.setDate(CurrencyService.convertStringToLocalDate(convertDtoWeb.getDate()));
        save(historyServiceDto);
    }

}
