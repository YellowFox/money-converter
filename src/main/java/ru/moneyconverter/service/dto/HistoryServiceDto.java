package ru.moneyconverter.service.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.time.LocalDate;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class HistoryServiceDto {

    private Long id;
    private CurrencyServiceDto currencyOne; //из какой валюты
    private CurrencyServiceDto currencyTwo; //в какую валюту
    private LocalDate date; //дата конвертации
}
