package ru.moneyconverter.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.Date;

@Data
@Entity
@Table(name = "history_tbl", schema = "money_shema")
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class HistoryModel {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_history")
    private Long id;

    @ManyToOne
    @JoinColumn(name = "currency_one_id")
    private CurrencyModel currencyOne; //из какой валюты

    @ManyToOne
    @JoinColumn(name = "currency_two_id")
    private CurrencyModel currencyTwo; //в какую валюту

    @Column(name = "date")
    private LocalDate date; //дата конвертации
}
