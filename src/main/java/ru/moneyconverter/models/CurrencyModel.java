package ru.moneyconverter.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

@Data
@Entity
@Table(name = "currencies_tbl", schema="money_shema")
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class CurrencyModel {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_currency")
    private Long id;
    @Column(name = "charcode")
    private String charCode; //символьное обозначение
    @Column(name = "nominal")
    private Integer nominal;
    @Column(name = "name")
    private String name; //полное название валюты
    @Column(name = "rate")
    private BigDecimal rate; //курс, сколько за 1 шт этой валюты дают в рублях
    @Column(name = "date")
    private Date date; //актуальность, каого чсла был курс актуален

}
